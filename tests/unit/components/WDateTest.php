<?php

namespace tests\models;


use app\components\WDate;

use Codeception\Test\Unit;


class WDateTest extends Unit
{
    public $dates = [
        '01:00:05 21.07.2017',
        '01:05 21.07.2017',
        '01: 21.07.2017',
        '21.07.2017',
        '07.2017',
        '2017',
        '01:',
        '01:05',
        '01:05:17',
    ];


    public function testDateCreate()
    {
        foreach ($this->dates as $date) {
            $this->assertObjectHasAttribute('dt',  new WDate($date));
        }
    }

    public function testDateCompare()
    {
        foreach ($this->dates as $date1) {

            $wd1 = new WDate($date1);

            foreach ($this->dates as $date2) {

                $wd2 = new WDate($date2);

                if ($wd1->getWeight() > $wd2->getWeight()) {
                    $this->assertTrue($wd1->larger($wd2));
                } elseif ($wd1->getWeight() < $wd2->getWeight()) {
                    $this->assertTrue($wd1->smaller($wd2));
                } else {
                    $this->assertTrue($wd1->equally($wd2));
                }

            }
        }
    }
}
