<?php

namespace app\components;

/**
 * Class WDate
 */
class WDate
{
    protected static $formatsWeights = [
        'H:i:s d.m.Y' => 100,
        'H:i d.m.Y' => 90,
        'H: d.m.Y' => 80,
        'd.m.Y'  => 70,
        'H:i:s' => 60,
        'm.Y' => 50,
        'H:i' => 40,
        'Y' => 30,
        'H:' => 20,
    ];

    /** @var \DateTime  */
    protected $dt;

    /** @var string */
    protected $format;

    /** @var int */
    protected $weight;


    public function __construct(string $dateStr)
    {
        foreach (self::$formatsWeights as $format => $weight) {

            $dt = \DateTime::createFromFormat($format, $dateStr);

            if ($dt) {
                $this->dt = $dt;
                $this->format = $format;
                $this->weight = $weight;

                break;
            }
        }

        if (!$this->dt) {
            throw new \Exception('Unsupported date format');
        }
    }

    public function __toString()
    {
        return $this->dt->format($this->format);
    }

    public function larger(WDate $wDate): bool
    {
        return $this->weight > $wDate->getWeight();
    }

    public function smaller(WDate $wDate): bool
    {
        return $this->weight < $wDate->getWeight();
    }

    public function equally(WDate $wDate): bool
    {
        return $this->weight == $wDate->getWeight();
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return array
     */
    public static function getFormatsWeights(): array
    {
        return self::$formatsWeights;
    }
}